# Overview

## Main feature

Extracting data from different data providers in a desirable format.

## Architecture

Service provides REST endpoint which allows to export requested data type in given CSV format. Data is fetched by external data providers that are easily attachable.

# Supported data

Given data types are supported:

- position

### Position
   
Position data is fetched from external data-generator service which can be found in [data-generator repo](https://bitbucket.org/grarkydev/data-generator).

`data-generator` generates each time randomly 100 positions that are translated to given schema:
```json
{
    "key": "awGUhfrouY",
    "name": "zenpiNfa",
    "fullName": "IkKRtixUNHVmNzAxeJTQpiMI",
    "country": "esM",
    "inEurope": false,
    "countryCode": "UK",
    "coreCountry": false,
    "distance": 6770,
    "_type": "Position",
    "_id": 0,
    "iata_airport_code": "cDtJfgXLwO",
    "type": "location",
    "latitude": 26,
    "longitude": 72,
    "location_id": 4712
}
```

# Data extracting

## Static

### Endpoint

`GET` request can be send to `/extract/static` path to extract `position` data in fixed format.

### Example request

Request:

`GET host:port/extract/static`

Result:
```
_type,_id,name,type,latitude,longitude
Position,0,xEDHnwmp,location,-9.087186,-18.168043
Position,1,MrbNfVHT,location,-66.82814,11.360413
Position,2,KDMDJRzJ,location,-30.706621,-112.730439
```

## Query

### Endpoint

`POST` request can be send to `/extract/query` path with query as a request body.

Query schema:
```json
{
	"resourceType": "requestType",
	"query": {
		"fields": ["firstField", "secondField"],
		"separator": ","
	}
}

```

Also `Content-Type: application/json` header should be attached to request.

### Example request

Request:

`POST host:port/extract/query` 

with body:

```json
{
	"resourceType": "position",
	"query": {
		"fields": ["name", "fullName", "_id", "country", "latitude", "longitude", "countryCode", "inEurope"],
		"separator": ","
	}
}

```

Result:
```
name,fullName,_id,country,latitude,longitude,countryCode,inEurope
zqwNJqnT,yicgKFFjMTcToizQDWVIwSUt,0,UIG,82.837229,-4.617635,pp,true
lCbFvVDZ,pbnXKyUAnRdDAWMSqTrqWtGM,1,ANr,22.110324,-52.983999,yg,true
CHXGoNnL,QgNXbnDoulCIutIhQftxslMr,2,mpd,-86.169772,48.60657,Km,false
```

# Building

Service is build with Spring Boot as a Maven project, so can be build with a Maven command:

`mvn clean package`

# Running

Service can be launched with Maven command:

`mvn spring-boot:run`

Service by default starts on 8080 port (this can be changed in `application.yml` file).
