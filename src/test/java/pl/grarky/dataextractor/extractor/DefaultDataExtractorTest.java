package pl.grarky.dataextractor.extractor;

import org.junit.Test;
import pl.grarky.dataextractor.csv.CsvQuery;
import pl.grarky.dataextractor.csv.CsvRequest;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.csv.exporter.CsvExporter;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvContentCreator;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvExporter;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvSchemaCreator;
import pl.grarky.dataextractor.data.provider.DataProviders;
import pl.grarky.dataextractor.data.provider.MockDataProvider;
import pl.grarky.dataextractor.data.provider.position.Position;
import pl.grarky.dataextractor.util.ExpectedCsvCreator;

import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.*;

public class DefaultDataExtractorTest {

    private final ExpectedCsvCreator expectedCsvCreator = new ExpectedCsvCreator();

    @Test
    public void shouldThrowInvalidCsvQueryExceptionOnceDataProviderCouldNotBeFoundByGivenResourceType() {
        DefaultDataExtractor dataExtractor = dataExtractorWith(
                alwaysInvalidCsvQueryExceptionThrowingDataProviders(),
                dummyCsvExporter());

        assertThatThrownBy(() -> dataExtractor.extractCsvFor(csvRequestWithResourceType("TYPE")))
                .isInstanceOf(InvalidCsvQueryException.class);
    }

    @Test
    public void shouldThrowCsvExportExceptionOnceCsvExportCannotBeFinished() {
        DefaultDataExtractor dataExtractor = dataExtractorWith(
                dataProvidersSupportingAndReturning(dummyResourceType(), emptyList()),
                jacksonCsvExporter());

        assertThatThrownBy(() -> dataExtractor.extractCsvFor(csvRequestWithResourceType(dummyResourceType())))
                .isInstanceOf(CsvExportException.class);
    }

    @Test
    public void shouldReturnValidExportedCsvContentAccordingToDataAndRequest() throws InvalidCsvQueryException, CsvExportException {
        String resourceType = "RESOURCE";
        List<Position> data = asList(warsaw(), paris(), london());
        Character separator = ';';
        List<String> fieldsToExtract = asList("key", "name");
        List<List<String>> expectedValues = expectedValuesFrom(data, asList(Position::getKey, Position::getName));

        DefaultDataExtractor dataExtractor = dataExtractorWith(
                dataProvidersSupportingAndReturning(resourceType, data),
                jacksonCsvExporter());

        String extractedCsv = dataExtractor.extractCsvFor(
                csvRequestWith(resourceType, fieldsToExtract, separator));

        String expectedCsv = expectedCsvCreator.expectedCsv(fieldsToExtract, separator, expectedValues);

        assertThat(extractedCsv).isEqualTo(expectedCsv);
    }

    private DefaultDataExtractor dataExtractorWith(DataProviders dataProviders, CsvExporter csvExporter) {
        return new DefaultDataExtractor(dataProviders, csvExporter);
    }

    private JacksonCsvExporter jacksonCsvExporter() {
        JacksonCsvSchemaCreator jacksonCsvSchemaCreator = new JacksonCsvSchemaCreator();
        JacksonCsvContentCreator jacksonCsvContentCreator = new JacksonCsvContentCreator();
        return new JacksonCsvExporter(jacksonCsvSchemaCreator, jacksonCsvContentCreator);
    }

    private DataProviders alwaysInvalidCsvQueryExceptionThrowingDataProviders() {
        return new DataProviders(emptyList());
    }

    private DataProviders dataProvidersSupportingAndReturning(String resourceType, List<?> returningData) {
        return new DataProviders(singletonList(new MockDataProvider(resourceType, returningData)));
    }

    private CsvExporter dummyCsvExporter() {
        return null;
    }

    private CsvRequest csvRequestWithResourceType(String resourceType) {
        return new CsvRequest(resourceType, new CsvQuery(emptyList(), ','));
    }

    private CsvRequest csvRequestWith(String resourceType, List<String> columns, Character separator) {
        return new CsvRequest(resourceType, new CsvQuery(columns, separator));
    }

    private String dummyResourceType() {
        return "DUMMY";
    }

    private List<List<String>> expectedValuesFrom(List<Position> data, List<Function<Position, String>> functions) {
        return data.stream()
                .map(entry -> functions.stream().map(function -> function.apply(entry)).collect(toList()))
                .collect(toList());
    }

}
