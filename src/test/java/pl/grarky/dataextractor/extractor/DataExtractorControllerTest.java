package pl.grarky.dataextractor.extractor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import pl.grarky.dataextractor.DataExtractorApplication;
import pl.grarky.dataextractor.csv.CsvQuery;
import pl.grarky.dataextractor.csv.CsvRequest;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.utils.StaticDataExtractorValues;

import static java.util.Collections.emptyList;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DataExtractorApplication.class)
@WebAppConfiguration
public class DataExtractorControllerTest {

    private MockMvc mockMvc;

    @Mock
    private DataExtractor dataExtractor;

    @Mock
    private StaticDataExtractorValues staticDataExtractorValues;

    @InjectMocks
    private DataExtractorController dataExtractorController;

    @Before
    public void initialize() {
        mockMvc = standaloneSetup(dataExtractorController).build();
    }

    @Test
    public void shouldReturnValidExtractedCsvContentWithOkStatusForStaticPath() throws Exception {
        shouldReturnValidExtractedCsvContentWithOkStatusFor(getFor("/extract/static"));
    }

    @Test
    public void shouldReturnValidExtractedCsvContentWithOkStatusForQueryPath() throws Exception {
        shouldReturnValidExtractedCsvContentWithOkStatusFor(postFor("/extract/query"));
    }

    @Test
    public void shouldReturnBadRequestExceptionWithBadRequestStatusOnceQueryCouldNotBeProcessedForStaticPath() throws Exception {
        shouldReturnBadRequestExceptionWithBadRequestStatusOnceQueryCouldNotBeProcessedFor(getFor("/extract/static"));
    }

    @Test
    public void shouldReturnBadRequestExceptionWithBadRequestStatusOnceQueryCouldNotBeProcessedForQueryPath() throws Exception {
        shouldReturnBadRequestExceptionWithBadRequestStatusOnceQueryCouldNotBeProcessedFor(postFor("/extract/query"));
    }

    @Test
    public void shouldReturnCsvExportExceptionWithUnprocessableEntityStatusOnceCsvExtractionCouldNotBeProcessedForStaticPath() throws Exception {
        shouldReturnCsvExportExceptionWithUnprocessableEntityStatusOnceCsvExtractionCouldNotBeProcessedFor(getFor("/extract/static"));
    }

    @Test
    public void shouldReturnCsvExportExceptionWithUnprocessableEntityStatusOnceCsvExtractionCouldNotBeProcessedForQueryPath() throws Exception {
        shouldReturnCsvExportExceptionWithUnprocessableEntityStatusOnceCsvExtractionCouldNotBeProcessedFor(postFor("/extract/query"));
    }

    private void shouldReturnValidExtractedCsvContentWithOkStatusFor(MockHttpServletRequestBuilder request) throws Exception {
        String extractedCsv = "extracted;csv;content";

        when(dataExtractor.extractCsvFor(any())).thenReturn(extractedCsv);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string(extractedCsv));
    }

    private void shouldReturnBadRequestExceptionWithBadRequestStatusOnceQueryCouldNotBeProcessedFor(MockHttpServletRequestBuilder request) throws Exception {
        when(dataExtractor.extractCsvFor(any())).thenThrow(new InvalidCsvQueryException("invalid query"));

        mockMvc.perform(request)
                .andExpect(status().isBadRequest());
    }

    private void shouldReturnCsvExportExceptionWithUnprocessableEntityStatusOnceCsvExtractionCouldNotBeProcessedFor(MockHttpServletRequestBuilder request) throws Exception {
        when(dataExtractor.extractCsvFor(any())).thenThrow(new CsvExportException());

        mockMvc.perform(request)
                .andExpect(status().isUnprocessableEntity());
    }

    private MockHttpServletRequestBuilder getFor(String path) {
        return get(path);
    }

    private MockHttpServletRequestBuilder postFor(String path) throws JsonProcessingException {
        return post(path)
                .content(jsonFrom(dummyCsvRequest()))
                .contentType(APPLICATION_JSON);
    }

    private CsvRequest dummyCsvRequest() {
        return new CsvRequest("resource", new CsvQuery(emptyList(), ';'));
    }

    private String jsonFrom(Object object) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(object);
    }

}
