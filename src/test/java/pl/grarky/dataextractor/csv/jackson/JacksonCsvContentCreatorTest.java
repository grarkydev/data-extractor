package pl.grarky.dataextractor.csv.jackson;

import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.junit.Test;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvContentCreator;
import pl.grarky.dataextractor.data.provider.position.Position;

import static com.fasterxml.jackson.dataformat.csv.CsvSchema.ColumnType.STRING;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.warsaw;

public class JacksonCsvContentCreatorTest {

    private JacksonCsvContentCreator jacksonCsvContentCreator;

    @Test
    public void shouldCreateCsvContentAccordingToSchema() throws CsvExportException {
        jacksonCsvContentCreator = new JacksonCsvContentCreator();

        Character separator = ';';
        Position warsaw = warsaw();
        CsvSchema schema = schemaFrom(separator, "name", "fullName", "_id", "_type");

        String expectedCsvContent = expectedContent(separator,
                warsaw.getName(), warsaw.getFullName(), valueOf(warsaw.getId()), warsaw.getEntityType());

        String exportedCsvContent = jacksonCsvContentCreator.csvContentFrom(singletonList(warsaw), schema);

        assertThat(exportedCsvContent).isEqualTo(expectedCsvContent);
    }

    private CsvSchema schemaFrom(Character separator, String... columns) {
        return CsvSchema.builder()
                .addColumns(asList(columns), STRING)
                .setColumnSeparator(separator)
                .build();
    }

    private String expectedContent(Character separator, String... values) {
        return stream(values).collect(joining(valueOf(separator), emptySeparator(), lineSeparator()));
    }

    private String lineSeparator() {
        return System.getProperty("line.separator");
    }

    private String emptySeparator() {
        return EMPTY;
    }

}
