package pl.grarky.dataextractor.csv.jackson;

import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.junit.Test;
import pl.grarky.dataextractor.csv.CsvQuery;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvSchemaCreator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class JacksonCsvSchemaCreatorTest {

    private final JacksonCsvSchemaCreator jacksonCsvSchemaCreator = new JacksonCsvSchemaCreator();

    @Test
    public void shouldCreateCsvSchemaContainingGivenColumns() {
        List<String> columns = asList("firstField", "secondField", "thirdField", "fourthField");
        CsvQuery query = new CsvQuery(columns, ',');

        CsvSchema csvSchema = jacksonCsvSchemaCreator.schemaFrom(query);

        assertThat(columnsFrom(csvSchema))
                .extracting(CsvSchema.Column::getName)
                .containsExactlyElementsOf(columns);
    }

    @Test
    public void shouldCreateCsvSchemaContainingGivenSeparator() {
        Character separator = ',';
        CsvQuery query = new CsvQuery(Collections.emptyList(), separator);

        CsvSchema csvSchema = jacksonCsvSchemaCreator.schemaFrom(query);

        assertThat(csvSchema.getColumnSeparator()).isEqualTo(separator);
    }

    private List<CsvSchema.Column> columnsFrom(CsvSchema csvSchema) {
        List<CsvSchema.Column> columns = new ArrayList<>();
        csvSchema.iterator().forEachRemaining(columns::add);
        return columns;
    }

}
