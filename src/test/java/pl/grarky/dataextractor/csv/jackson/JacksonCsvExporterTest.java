package pl.grarky.dataextractor.csv.jackson;

import org.junit.Test;
import pl.grarky.dataextractor.csv.CsvQuery;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvContentCreator;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvExporter;
import pl.grarky.dataextractor.csv.exporter.jackson.JacksonCsvSchemaCreator;
import pl.grarky.dataextractor.data.provider.position.Position;
import pl.grarky.dataextractor.util.ExpectedCsvCreator;

import java.util.List;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.*;

public class JacksonCsvExporterTest {

    private final ExpectedCsvCreator expectedCsvCreator = new ExpectedCsvCreator();

    @Test
    public void shouldCorrectlyExportCsvDataFromGivenObjectAndSchema() throws CsvExportException {
        JacksonCsvExporter jacksonCsvExporter = jacksonCsvExporter();

        Position warsaw = warsaw();
        List<String> fieldsToExtract = asList("_id", "_type", "name", "fullName");
        Character separator = ',';
        List<List<String>> expectedValues = singletonList(
                asList(valueOf(warsaw.getId()), warsaw.getEntityType(), warsaw.getName(), warsaw.getFullName())
        );

        List<?> data = singletonList(warsaw);
        CsvQuery query = queryWith(separator, fieldsToExtract);

        String exportedCsv = jacksonCsvExporter.exportToCsv(data, query);
        String expectedCsv = expectedCsvCreator.expectedCsv(fieldsToExtract, separator, expectedValues);

        assertThat(exportedCsv).isEqualTo(expectedCsv);
    }

    @Test
    public void shouldCorrectlyExportCsvDataFromGivenObjectsAndSchema() throws CsvExportException {
        JacksonCsvExporter jacksonCsvExporter = jacksonCsvExporter();

        Position warsaw = warsaw();
        Position london = london();
        Position paris = paris();
        List<String> fieldsToExtract = asList("key", "name");
        Character separator = ';';
        List<List<String>> expectedValues = asList(
                asList(warsaw.getKey(), warsaw.getName()),
                asList(paris.getKey(), paris.getName()),
                asList(london.getKey(), london.getName())
        );

        List<?> data = asList(warsaw, paris, london);
        CsvQuery query = queryWith(separator, fieldsToExtract);

        String exportedCsv = jacksonCsvExporter.exportToCsv(data, query);
        String expectedCsv = expectedCsvCreator.expectedCsv(fieldsToExtract, separator, expectedValues);

        assertThat(exportedCsv).isEqualTo(expectedCsv);
    }

    private JacksonCsvExporter jacksonCsvExporter() {
        JacksonCsvSchemaCreator jacksonCsvSchemaCreator = new JacksonCsvSchemaCreator();
        JacksonCsvContentCreator jacksonCsvContentCreator = new JacksonCsvContentCreator();
        return new JacksonCsvExporter(jacksonCsvSchemaCreator, jacksonCsvContentCreator);
    }

    private CsvQuery queryWith(Character separator, List<String> fields) {
        return new CsvQuery(fields, separator);
    }

}
