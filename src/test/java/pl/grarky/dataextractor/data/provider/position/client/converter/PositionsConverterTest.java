package pl.grarky.dataextractor.data.provider.position.client.converter;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.*;

public class PositionsConverterTest {

    private final PositionsConverter positionsConverter = new PositionsConverter(new PositionConverter());

    @Test
    public void shouldCorrectlyConvertPositionJsonsToPositionsList() {
        assertThat(positionsConverter.fromJsons(asList(warsawJson(), parisJson(), londonJson())))
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactly(warsaw(), paris(), london());
    }

}
