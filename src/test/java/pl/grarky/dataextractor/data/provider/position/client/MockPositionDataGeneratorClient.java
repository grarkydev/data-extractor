package pl.grarky.dataextractor.data.provider.position.client;

import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

public class MockPositionDataGeneratorClient implements PositionDataGeneratorClient {

    private final List<PositionJson> returningPositions;

    public MockPositionDataGeneratorClient(List<PositionJson> returningPositions) {
        this.returningPositions = returningPositions;
    }

    @Override
    public List<PositionJson> generatePositions(Integer size) {
        return returningPositions;
    }
}
