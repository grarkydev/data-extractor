package pl.grarky.dataextractor.data.provider.position;

import pl.grarky.dataextractor.data.provider.position.client.model.GeographicPositionJson;
import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.math.BigDecimal;

import static pl.grarky.dataextractor.util.LondonData.*;
import static pl.grarky.dataextractor.util.ParisData.*;
import static pl.grarky.dataextractor.util.WarsawData.*;

public class PositionsProvider {

    public static Position warsaw() {
        return positionFrom(warsawEntityType, warsawId, warsawKey, warsawName, warsawFullName, warsawAirportCode,
                warsawLocationType, warsawCountry, warsawLatitude, warsawLongitude, warsawLocationId,
                warsawInEurope, warsawCountryCode, warsawCoreCountry, warsawDistance
        );
    }

    public static PositionJson warsawJson() {
        return positionJsonFrom(warsawEntityType, warsawId, warsawKey, warsawName, warsawFullName, warsawAirportCode,
                warsawLocationType, warsawCountry, warsawLatitude, warsawLongitude, warsawLocationId,
                warsawInEurope, warsawCountryCode, warsawCoreCountry, warsawDistance
        );
    }

    public static Position london() {
        return positionFrom(londonEntityType, londonId, londonKey, londonName, londonFullName, londonAirportCode,
                londonLocationType, londonCountry, londonLatitude, londonLongitude, londonLocationId,
                londonInEurope, londonCountryCode, londonCoreCountry, londonDistance
        );
    }

    public static PositionJson londonJson() {
        return positionJsonFrom(londonEntityType, londonId, londonKey, londonName, londonFullName, londonAirportCode,
                londonLocationType, londonCountry, londonLatitude, londonLongitude, londonLocationId,
                londonInEurope, londonCountryCode, londonCoreCountry, londonDistance
        );
    }

    public static Position paris() {
        return positionFrom(parisEntityType, parisId, parisKey, parisName, parisFullName, parisAirportCode,
                parisLocationType, parisCountry, parisLatitude, parisLongitude, parisLocationId,
                parisInEurope, parisCountryCode, parisCoreCountry, parisDistance
        );
    }

    public static PositionJson parisJson() {
        return positionJsonFrom(parisEntityType, parisId, parisKey, parisName, parisFullName, parisAirportCode,
                parisLocationType, parisCountry, parisLatitude, parisLongitude, parisLocationId,
                parisInEurope, parisCountryCode, parisCoreCountry, parisDistance
        );
    }

    private static Position positionFrom(String entityType, Long id, String key, String name, String fullName, String airportCode,
                                         String locationType, String country, Double latitude, Double longitude, Long locationId,
                                         Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        return new Position(entityType, id, key, name, fullName, airportCode, locationType,
                country, latitude, longitude, locationId, inEurope, countryCode, coreCountry, distance
        );
    }

    private static PositionJson positionJsonFrom(String entityType, Long id, String key, String name, String fullName, String airportCode,
                                                 String locationType, String country, Double latitude, Double longitude, Long locationId,
                                                 Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        return new PositionJson(entityType, id, key, name, fullName, airportCode, locationType,
                country, new GeographicPositionJson(latitude, longitude), locationId, inEurope, countryCode,
                coreCountry, distance
        );
    }

}
