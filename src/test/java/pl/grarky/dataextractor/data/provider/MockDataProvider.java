package pl.grarky.dataextractor.data.provider;

import java.util.List;

public class MockDataProvider implements DataProvider {

    private final String supportedResourceType;
    private final List<?> returningData;

    public MockDataProvider(String supportedResourceType, List<?> returningData) {
        this.supportedResourceType = supportedResourceType;
        this.returningData = returningData;
    }

    @Override
    public boolean supports(String resourceType) {
        return supportedResourceType.equals(resourceType);
    }

    @Override
    public List<?> provideData() {
        return returningData;
    }

}
