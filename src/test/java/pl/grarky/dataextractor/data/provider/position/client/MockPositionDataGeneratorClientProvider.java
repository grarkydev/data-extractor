package pl.grarky.dataextractor.data.provider.position.client;

import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

public class MockPositionDataGeneratorClientProvider implements PositionDataGeneratorClientProvider {

    private final PositionDataGeneratorClient positionDataGeneratorClient;

    public MockPositionDataGeneratorClientProvider(List<PositionJson> returningData) {
        positionDataGeneratorClient = new MockPositionDataGeneratorClient(returningData);
    }

    @Override
    public PositionDataGeneratorClient positionDataGeneratorClient() {
        return positionDataGeneratorClient;
    }

}
