package pl.grarky.dataextractor.data.provider.position.client.converter;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.warsaw;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.warsawJson;

public class PositionConverterTest {

    private final PositionConverter positionConverter = new PositionConverter();

    @Test
    public void shouldCorrectlyConvertPositionJsonToPosition() {
        assertThat(positionConverter.fromJson(warsawJson()))
                .isEqualToComparingFieldByFieldRecursively(warsaw());
    }

}
