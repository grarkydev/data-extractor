package pl.grarky.dataextractor.data.provider;

import org.junit.Before;
import org.junit.Test;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class DataProvidersTest {

    private final String SUPPORTED_TYPE = "SUPPORTED";
    private final String UNSUPPORTED_TYPE = "UNSUPPORTED";

    private DataProviders dataProviders;
    private DataProvider mockDataProvider;

    @Before
    public void configureDataProviders() {
        mockDataProvider = new MockDataProvider(SUPPORTED_TYPE, emptyList());
        dataProviders = new DataProviders(singletonList(mockDataProvider));
    }

    @Test
    public void shouldGiveDataProviderWhichSupportsGivenResourceType() throws UnsupportedResourceTypeException {
        assertThat(dataProviders.dataProviderFor(SUPPORTED_TYPE))
                .isEqualTo(mockDataProvider);
    }

    @Test
    public void shouldThrowUnsupportedResourceTypeExceptionOnceDataProviderCouldNotBeFoundByGivenResourceType() {
        assertThatThrownBy(() -> dataProviders.dataProviderFor(UNSUPPORTED_TYPE))
                .isInstanceOf(UnsupportedResourceTypeException.class);
    }


}
