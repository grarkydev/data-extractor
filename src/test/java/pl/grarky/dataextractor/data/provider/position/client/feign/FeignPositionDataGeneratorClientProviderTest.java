package pl.grarky.dataextractor.data.provider.position.client.feign;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FeignPositionDataGeneratorClientProviderTest {

    @Test
    public void shouldReturnFeignPositionDataGeneratorClientImplementation() {
        assertThat(configuredFeignPositionDataGeneratorClientProvider().positionDataGeneratorClient())
                .isInstanceOf(FeignPositionDataGeneratorClient.class);
    }

    private FeignPositionDataGeneratorClientProvider configuredFeignPositionDataGeneratorClientProvider() {
        FeignPositionDataGeneratorClientProvider feignPositionDataGeneratorClientProvider =
                new FeignPositionDataGeneratorClientProvider("address");

        feignPositionDataGeneratorClientProvider.initializeAndConfigurePositionGeneratorServiceClient();

        return feignPositionDataGeneratorClientProvider;
    }

}
