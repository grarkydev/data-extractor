package pl.grarky.dataextractor.data.provider.position;

import org.junit.Test;
import pl.grarky.dataextractor.data.provider.position.client.MockPositionDataGeneratorClientProvider;
import pl.grarky.dataextractor.data.provider.position.client.converter.PositionConverter;
import pl.grarky.dataextractor.data.provider.position.client.converter.PositionsConverter;
import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.dataextractor.data.provider.position.PositionsProvider.*;

public class PositionDataProviderTest {

    @Test
    public void shouldGenerateValidPositions() {
        PositionDataProvider positionDataProvider = positionDataProviderReturning(warsawJson(), parisJson());
        List<Position> expectedPositions = asList(warsaw(), paris());

        List<Position> providedPositions = (List<Position>) positionDataProvider.provideData();

        assertThat(providedPositions)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyElementsOf(expectedPositions);
    }

    @Test
    public void shouldSupportResourceTypeOnceResourceTypeMatchesSupportedResourceType() {
        String resourceType = "SUPPORTED";
        PositionDataProvider positionDataProvider = positionDataProviderWithType(resourceType);

        assertThat(positionDataProvider.supports(resourceType)).isTrue();
    }

    @Test
    public void shouldNotSupportResourceTypeOnceResourceTypeDoesNotMatchSupportedResourceType() {
        String supportedResourceType = "SUPPORTED";
        String unsupportedResourceType = "UNSUPPORTED";
        PositionDataProvider positionDataProvider = positionDataProviderWithType(supportedResourceType);

        assertThat(positionDataProvider.supports(unsupportedResourceType)).isFalse();
    }

    private PositionDataProvider positionDataProviderReturning(PositionJson... positionJsons) {
        return new PositionDataProvider("TYPE", 100,
                new MockPositionDataGeneratorClientProvider(asList(positionJsons)),
                new PositionsConverter(new PositionConverter())
        );
    }

    private PositionDataProvider positionDataProviderWithType(String resourceType) {
        return new PositionDataProvider(resourceType, 100,
                new MockPositionDataGeneratorClientProvider(emptyList()),
                new PositionsConverter(new PositionConverter())
        );
    }

}
