package pl.grarky.dataextractor.util;

import java.util.List;

import static java.lang.String.valueOf;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang.StringUtils.EMPTY;

public class ExpectedCsvCreator {

    public String expectedCsv(List<String> fields, Character separator, List<List<String>> expectedValues) {
        return headerFrom(fields, separator) +
                dataValuesFrom(expectedValues, separator);
    }

    private String headerFrom(List<String> fields, Character separator) {
        return fields.stream().collect(joining(valueOf(separator))) + lineSeparator();
    }

    private String dataValuesFrom(List<List<String>> expectedValues, Character separator) {
        return expectedValues.stream()
                .map(line -> line.stream().collect(joining(valueOf(separator))))
                .collect(joining(lineSeparator(), emptySeparator(), lineSeparator()));
    }

    private String lineSeparator() {
        return System.getProperty("line.separator");
    }

    private String emptySeparator() {
        return EMPTY;
    }

}
