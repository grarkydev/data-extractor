package pl.grarky.dataextractor.csv;

import java.util.List;

public class CsvQuery {

    private List<String> fields;
    private Character separator;

    public CsvQuery() {

    }

    public CsvQuery(List<String> fields, Character separator) {
        this.fields = fields;
        this.separator = separator;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public Character getSeparator() {
        return separator;
    }

    public void setSeparator(Character separator) {
        this.separator = separator;
    }

}
