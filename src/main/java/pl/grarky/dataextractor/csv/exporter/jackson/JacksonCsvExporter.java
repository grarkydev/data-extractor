package pl.grarky.dataextractor.csv.exporter.jackson;

import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.csv.CsvQuery;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.csv.exporter.CsvExporter;

import java.util.List;

@Component
public class JacksonCsvExporter implements CsvExporter {

    private final JacksonCsvSchemaCreator jacksonCsvSchemaCreator;
    private final JacksonCsvContentCreator jacksonCsvContentCreator;

    @Autowired
    public JacksonCsvExporter(JacksonCsvSchemaCreator jacksonCsvSchemaCreator,
                              JacksonCsvContentCreator jacksonCsvContentCreator) {
        this.jacksonCsvSchemaCreator = jacksonCsvSchemaCreator;
        this.jacksonCsvContentCreator = jacksonCsvContentCreator;
    }

    @Override
    public String exportToCsv(List<?> data, CsvQuery csvQuery) throws CsvExportException {
        CsvSchema csvSchema = jacksonCsvSchemaCreator.schemaFrom(csvQuery);
        return jacksonCsvContentCreator.csvContentFrom(data, csvSchema);
    }

}
