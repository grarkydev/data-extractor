package pl.grarky.dataextractor.csv.exporter.jackson;

import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.csv.CsvQuery;

import static com.fasterxml.jackson.dataformat.csv.CsvSchema.ColumnType.STRING;

@Component
public class JacksonCsvSchemaCreator {

    public CsvSchema schemaFrom(CsvQuery csvQuery) {
        return CsvSchema.builder()
                .addColumns(csvQuery.getFields(), STRING)
                .setColumnSeparator(csvQuery.getSeparator())
                .setUseHeader(true)
                .build();
    }

}
