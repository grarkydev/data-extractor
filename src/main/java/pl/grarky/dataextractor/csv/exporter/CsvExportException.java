package pl.grarky.dataextractor.csv.exporter;

public class CsvExportException extends Exception {

    public CsvExportException() {

    }

    public CsvExportException(Throwable cause) {
        super("Unable to export given data to CSV file", cause);
    }

}
