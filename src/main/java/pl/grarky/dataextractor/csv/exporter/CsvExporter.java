package pl.grarky.dataextractor.csv.exporter;

import pl.grarky.dataextractor.csv.CsvQuery;

import java.util.List;

public interface CsvExporter {

    String exportToCsv(List<?> data, CsvQuery csvQuery) throws CsvExportException;

}
