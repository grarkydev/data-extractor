package pl.grarky.dataextractor.csv.exporter.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;

import java.util.List;

import static com.fasterxml.jackson.core.JsonGenerator.Feature.IGNORE_UNKNOWN;

@Component
public class JacksonCsvContentCreator {

    private final CsvMapper csvMapper;

    @Autowired
    public JacksonCsvContentCreator() {
        csvMapper = new CsvMapper();
        csvMapper.enable(IGNORE_UNKNOWN);
    }

    public String csvContentFrom(List<?> data, CsvSchema csvSchema) throws CsvExportException {
        try {
            return csvMapper.writer(csvSchema).writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new CsvExportException(e);
        }
    }

}
