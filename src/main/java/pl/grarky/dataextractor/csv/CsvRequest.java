package pl.grarky.dataextractor.csv;

import java.util.List;

public class CsvRequest {

    private String resourceType;
    private CsvQuery query;

    public CsvRequest() {

    }

    public CsvRequest(String resourceType, CsvQuery csvQuery) {
        this.resourceType = resourceType;
        this.query = csvQuery;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public CsvQuery getQuery() {
        return query;
    }

    public void setQuery(CsvQuery query) {
        this.query = query;
    }

}
