package pl.grarky.dataextractor.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Arrays.asList;

@Component
public class StaticDataExtractorValues {

    private final String resourceType;
    private final List<String> fields;
    private final Character separator;

    public StaticDataExtractorValues(
            @Value("${export.position.type}") String resourceType,
            @Value("${export.position.fields}") String[] fields,
            @Value("${export.position.separator}") Character separator) {
        this.resourceType = resourceType;
        this.fields = asList(fields);
        this.separator = separator;
    }

    public String getResourceType() {
        return resourceType;
    }

    public List<String> getFields() {
        return fields;
    }

    public Character getSeparator() {
        return separator;
    }

}
