package pl.grarky.dataextractor.extractor;

public class InvalidCsvQueryException extends Exception {

    public InvalidCsvQueryException(String message) {
        super(message);
    }

}
