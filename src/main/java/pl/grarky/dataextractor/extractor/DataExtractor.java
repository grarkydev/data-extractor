package pl.grarky.dataextractor.extractor;

import pl.grarky.dataextractor.csv.CsvRequest;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;

public interface DataExtractor {

    String extractCsvFor(CsvRequest csvRequest) throws InvalidCsvQueryException, CsvExportException;

}
