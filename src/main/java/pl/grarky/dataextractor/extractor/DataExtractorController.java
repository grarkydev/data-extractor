package pl.grarky.dataextractor.extractor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.grarky.dataextractor.csv.CsvQuery;
import pl.grarky.dataextractor.csv.CsvRequest;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.utils.StaticDataExtractorValues;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/extract")
public class DataExtractorController {

    private final DataExtractor dataExtractor;
    private final StaticDataExtractorValues staticDataExtractorValues;

    @Autowired
    public DataExtractorController(DataExtractor dataExtractor, StaticDataExtractorValues staticDataExtractorValues) {
        this.dataExtractor = dataExtractor;
        this.staticDataExtractorValues = staticDataExtractorValues;
    }

    @GetMapping("/static")
    public ResponseEntity<String> extractDataStatically() {
        return extractDataWithQuery(staticCsvRequest());
    }

    @PostMapping("/query")
    public ResponseEntity<String> extractDataWithQuery(@RequestBody CsvRequest csvRequest) {
        try {
            return ok(dataExtractor.extractCsvFor(csvRequest));
        } catch (InvalidCsvQueryException e) {
            throw new BadRequestException(e);
        } catch (CsvExportException e) {
            throw new ProcessingException(e);
        }
    }

    private CsvRequest staticCsvRequest() {
        return new CsvRequest(
                staticDataExtractorValues.getResourceType(),
                new CsvQuery(
                        staticDataExtractorValues.getFields(),
                        staticDataExtractorValues.getSeparator()
                )
        );
    }

    @ResponseStatus(BAD_REQUEST)
    private class BadRequestException extends RuntimeException {

        private BadRequestException(Throwable cause) {
            super(cause);
        }

    }

    @ResponseStatus(UNPROCESSABLE_ENTITY)
    private class ProcessingException extends RuntimeException {

        private ProcessingException(Throwable cause) {
            super(cause);
        }

    }

}