package pl.grarky.dataextractor.extractor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.csv.CsvRequest;
import pl.grarky.dataextractor.csv.exporter.CsvExportException;
import pl.grarky.dataextractor.csv.exporter.CsvExporter;
import pl.grarky.dataextractor.data.provider.DataProviders;

import java.util.List;

@Component
public class DefaultDataExtractor implements DataExtractor {

    private final DataProviders dataProviders;
    private final CsvExporter csvExporter;

    @Autowired
    public DefaultDataExtractor(DataProviders dataProviders, CsvExporter csvExporter) {
        this.dataProviders = dataProviders;
        this.csvExporter = csvExporter;
    }

    @Override
    public String extractCsvFor(CsvRequest csvRequest) throws InvalidCsvQueryException, CsvExportException {
        List<?> data = dataProviders.dataProviderFor(csvRequest.getResourceType()).provideData();
        return csvExporter.exportToCsv(data, csvRequest.getQuery());
    }

}
