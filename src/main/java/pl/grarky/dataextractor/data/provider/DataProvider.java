package pl.grarky.dataextractor.data.provider;

import java.util.List;

public interface DataProvider {

    boolean supports(String resourceType);

    List<?> provideData();

}
