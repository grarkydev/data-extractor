package pl.grarky.dataextractor.data.provider.position.client.converter;

import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.data.provider.position.Position;
import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

@Component
public class PositionConverter {

    public Position fromJson(PositionJson positionJson) {
        return new Position(
                positionJson.getEntityType(),
                positionJson.getId(),
                positionJson.getKey(),
                positionJson.getName(),
                positionJson.getFullName(),
                positionJson.getAirportCode(),
                positionJson.getLocationType(),
                positionJson.getCountry(),
                positionJson.getGeographicPosition().getLatitude(),
                positionJson.getGeographicPosition().getLongitude(),
                positionJson.getLocationId(),
                positionJson.getInEurope(),
                positionJson.getCountryCode(),
                positionJson.getCoreCountry(),
                positionJson.getDistance()
        );
    }

}
