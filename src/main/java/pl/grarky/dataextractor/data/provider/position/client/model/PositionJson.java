package pl.grarky.dataextractor.data.provider.position.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class PositionJson {

    @JsonProperty("_type")
    private String entityType;

    @JsonProperty("_id")
    private Long id;

    private String key;

    private String name;

    private String fullName;

    @JsonProperty("iata_airport_code")
    private String airportCode;

    @JsonProperty("type")
    private String locationType;

    private String country;

    @JsonProperty("geo_position")
    private GeographicPositionJson geographicPosition;

    @JsonProperty("location_id")
    private Long locationId;

    private Boolean inEurope;

    private String countryCode;

    private Boolean coreCountry;

    private BigDecimal distance;

    public PositionJson() {

    }

    public PositionJson(String entityType, Long id, String key, String name, String fullName, String airportCode,
                        String locationType, String country, GeographicPositionJson geographicPosition,
                        Long locationId, Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        this.entityType = entityType;
        this.id = id;
        this.key = key;
        this.name = name;
        this.fullName = fullName;
        this.airportCode = airportCode;
        this.locationType = locationType;
        this.country = country;
        this.geographicPosition = geographicPosition;
        this.locationId = locationId;
        this.inEurope = inEurope;
        this.countryCode = countryCode;
        this.coreCountry = coreCountry;
        this.distance = distance;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public GeographicPositionJson getGeographicPosition() {
        return geographicPosition;
    }

    public void setGeographicPosition(GeographicPositionJson geographicPosition) {
        this.geographicPosition = geographicPosition;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Boolean getInEurope() {
        return inEurope;
    }

    public void setInEurope(Boolean inEurope) {
        this.inEurope = inEurope;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getCoreCountry() {
        return coreCountry;
    }

    public void setCoreCountry(Boolean coreCountry) {
        this.coreCountry = coreCountry;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }
}
