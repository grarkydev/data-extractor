package pl.grarky.dataextractor.data.provider.position;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Position {

    @JsonProperty("_type")
    private final String entityType;

    @JsonProperty("_id")
    private final Long id;

    private final String key;

    private final String name;

    private final String fullName;

    @JsonProperty("iata_airport_code")
    private final String airportCode;

    @JsonProperty("type")
    private final String locationType;

    private final String country;

    private final Double latitude;

    private final Double longitude;

    @JsonProperty("location_id")
    private final Long locationId;

    private final Boolean inEurope;

    private final String countryCode;

    private final Boolean coreCountry;

    private final BigDecimal distance;

    public Position(String entityType, Long id, String key, String name, String fullName, String airportCode,
                    String locationType, String country, Double latitude, Double longitude, Long locationId,
                    Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        this.entityType = entityType;
        this.id = id;
        this.key = key;
        this.name = name;
        this.fullName = fullName;
        this.airportCode = airportCode;
        this.locationType = locationType;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.locationId = locationId;
        this.inEurope = inEurope;
        this.countryCode = countryCode;
        this.coreCountry = coreCountry;
        this.distance = distance;
    }

    public String getEntityType() {
        return entityType;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public String getLocationType() {
        return locationType;
    }

    public String getCountry() {
        return country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Long getLocationId() {
        return locationId;
    }

    public Boolean getInEurope() {
        return inEurope;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Boolean getCoreCountry() {
        return coreCountry;
    }

    public BigDecimal getDistance() {
        return distance;
    }

}
