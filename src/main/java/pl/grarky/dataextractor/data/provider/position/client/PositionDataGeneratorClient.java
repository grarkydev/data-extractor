package pl.grarky.dataextractor.data.provider.position.client;

import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

public interface PositionDataGeneratorClient {

    List<PositionJson> generatePositions(Integer size);

}
