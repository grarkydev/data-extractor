package pl.grarky.dataextractor.data.provider.position.client.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.data.provider.position.Position;
import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class PositionsConverter {

    private final PositionConverter positionConverter;

    @Autowired
    public PositionsConverter(PositionConverter positionConverter) {
        this.positionConverter = positionConverter;
    }

    public List<Position> fromJsons(List<PositionJson> positionJsons) {
        return positionJsons.stream()
                .map(positionConverter::fromJson)
                .collect(toList());
    }

}
