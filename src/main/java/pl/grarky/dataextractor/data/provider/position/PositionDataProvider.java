package pl.grarky.dataextractor.data.provider.position;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.data.provider.DataProvider;
import pl.grarky.dataextractor.data.provider.position.client.PositionDataGeneratorClientProvider;
import pl.grarky.dataextractor.data.provider.position.client.converter.PositionsConverter;
import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

@Component
public class PositionDataProvider implements DataProvider {

    private final String positionResourceType;
    private final Integer generatedPositionsAmount;
    private final PositionDataGeneratorClientProvider positionDataGeneratorClientProvider;
    private final PositionsConverter positionsConverter;

    public PositionDataProvider(
            @Value("${export.position.type}") String positionResourceType,
            @Value("${generator.position.amount}") Integer generatedPositionsAmount,
            PositionDataGeneratorClientProvider positionDataGeneratorClientProvider,
            PositionsConverter positionsConverter) {
        this.positionResourceType = positionResourceType;
        this.generatedPositionsAmount = generatedPositionsAmount;
        this.positionDataGeneratorClientProvider = positionDataGeneratorClientProvider;
        this.positionsConverter = positionsConverter;
    }

    @Override
    public boolean supports(String resourceType) {
        return resourceType != null
                && positionResourceType.toLowerCase().equals(resourceType.toLowerCase());
    }

    @Override
    public List<?> provideData() {
        return converted(positionsFromDataGeneratorService());
    }

    private List<PositionJson> positionsFromDataGeneratorService() {
        return positionDataGeneratorClientProvider
                .positionDataGeneratorClient()
                .generatePositions(generatedPositionsAmount);
    }

    private List<Position> converted(List<PositionJson> positionJsons) {
        return positionsConverter.fromJsons(positionJsons);
    }

}
