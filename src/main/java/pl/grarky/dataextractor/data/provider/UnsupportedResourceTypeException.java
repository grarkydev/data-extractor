package pl.grarky.dataextractor.data.provider;

import pl.grarky.dataextractor.extractor.InvalidCsvQueryException;

public class UnsupportedResourceTypeException extends InvalidCsvQueryException {

    public UnsupportedResourceTypeException() {
        super("Given resource type is not supported");
    }

}
