package pl.grarky.dataextractor.data.provider.position.client.feign;

import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import pl.grarky.dataextractor.data.provider.position.client.PositionDataGeneratorClient;
import pl.grarky.dataextractor.data.provider.position.client.model.PositionJson;

import java.util.List;

@FeignClient("position")
public interface FeignPositionDataGeneratorClient extends PositionDataGeneratorClient {

    @RequestLine("GET /generate/json/{size}")
    List<PositionJson> generatePositions(@Param("size") Integer size);

}
