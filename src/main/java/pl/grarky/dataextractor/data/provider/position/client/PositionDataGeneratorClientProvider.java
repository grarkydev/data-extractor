package pl.grarky.dataextractor.data.provider.position.client;

public interface PositionDataGeneratorClientProvider {

    PositionDataGeneratorClient positionDataGeneratorClient();

}
