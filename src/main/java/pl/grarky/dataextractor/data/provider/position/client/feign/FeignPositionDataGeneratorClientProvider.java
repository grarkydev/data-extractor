package pl.grarky.dataextractor.data.provider.position.client.feign;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.feign.support.ResponseEntityDecoder;
import org.springframework.stereotype.Component;
import pl.grarky.dataextractor.data.provider.position.client.PositionDataGeneratorClient;
import pl.grarky.dataextractor.data.provider.position.client.PositionDataGeneratorClientProvider;

import javax.annotation.PostConstruct;

@Component
public class FeignPositionDataGeneratorClientProvider implements PositionDataGeneratorClientProvider {

    private final String positionGeneratorServiceAddress;

    private PositionDataGeneratorClient positionDataGeneratorClient;

    public FeignPositionDataGeneratorClientProvider(
            @Value("${service.generator.address}") String positionGeneratorServiceAddress) {
        this.positionGeneratorServiceAddress = positionGeneratorServiceAddress;
    }

    @PostConstruct
    void initializeAndConfigurePositionGeneratorServiceClient() {
        positionDataGeneratorClient = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new ResponseEntityDecoder(new JacksonDecoder()))
                .target(FeignPositionDataGeneratorClient.class, positionGeneratorServiceAddress);
    }

    @Override
    public PositionDataGeneratorClient positionDataGeneratorClient() {
        return positionDataGeneratorClient;
    }

}
