package pl.grarky.dataextractor.data.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataProviders {

    private final List<DataProvider> dataProviders;

    @Autowired
    public DataProviders(List<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
    }

    public DataProvider dataProviderFor(String resourceType) throws UnsupportedResourceTypeException {
        return dataProviders.stream()
                .filter(dataProvider -> dataProvider.supports(resourceType))
                .findFirst()
                .orElseThrow(UnsupportedResourceTypeException::new);
    }

}
